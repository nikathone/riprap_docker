# Islandora Riprap docker

Containerizing the [Riprap](https://github.com/mjordan/riprap) microservice
using docker.

## Requirements

- [Git](https://git-scm.com/downloads)
- [Docker](https://www.docker.com/get-started)
- [Docker-compose](https://docs.docker.com/compose/install/) (optional)
- [Vagrant](https://www.vagrantup.com/downloads.html)
- [VirtualBox](https://www.virtualbox.org/wiki/Downloads)

## Getting started

### Installing and Configuring Islandora 8 RDM flavor Locally

1. Follow the instructions found on the [RDM Playbook](https://github.com/roblib/rdm-playbook)
   to get islandora 8 RDM flavor running locally.
2. Download and install the [Islandora riprap integration](https://github.com/mjordan/islandora_riprap).
3. Navigate to the [Islandora riprap](http://localhost:8000/admin/config/islandora_riprap/settings)
   module page settings
4. For _Riprap location_ select `Remote`
5. For _Riprap microservice REST endpoint_ enter `10.0.2.2:8001/api/fixity`
6. For _Gemini microservice REST endpoint_ leave it at `localhost:8000/gemini`
7. Consult the [README](https://github.com/mjordan/islandora_riprap#islandora-riprap)
   of the riprap drupal module, for further configurations information.

### Building and running the containerized riprap

1. Create and navigate into a folder named `riprap` for example
2. Clone the [riprap](https://github.com/mjordan/islandora_riprap) repo into this folder. `git clone git@github.com:mjordan/islandora_riprap.git code`
3. Clone this repository under a folder named `docker`
4. Copy the `.dockerignore` file from `docker/.dockerignore` into the current repository. `cp docker/.dockerignore .`
5. Build the docker image containing the riprap code by running the
   command below. If you plan to debug the riprap container based on this image,
   it might be good to change the `--target` flag and `--build-arg build_environment`
   value to `dev`.

```bash
docker image build --tag islandora_riprap:latest \
--target prod
--file docker/Dockerfile \
--build-arg code_dir=./code \
--build-arg build_environment=prod \
./
```

6. Run the container of the image built above with the following command:

```bash
docker container run --interactive --tty \
--port 8001:8001 \
--name islandora_riprap \
islandora_riprap:latest \
--detach
```

7. If you are on an Operating system which is not MacOS or Windows, you will
   need to add the `--network host` flag and adjust the config accordingly.
8. Commands can be executed from the host into the container with `docker container exec --interactive --tty islandora_riprap <command>`
9. Or Ssh into the container with `docker container exec --interactive --tty islandora_riprap bash`.

## Developing riprap

In a containerized environment can be achieve by using the [`docker-compose.yml`](./docker-compose.yml)
provided in this repo.

Once the development is done a docker image can be built and pushed to a
remote docker registry.

## Running the container on remote server

Ideally the image should be built using a continuous integration system which
can package the riprap code into the image and push into a remote
registry. Then the image could be used on the remote server to run the container.

## Current limitation

### Access control/Authentication

This image doesn't provide any access control for the API but since it's
running nginx as it's webserver we can extend it's configuration to include a
[basic auth](https://docs.nginx.com/nginx/admin-guide/security-controls/configuring-http-basic-authentication/)
or [JWT](https://docs.nginx.com/nginx/admin-guide/security-controls/configuring-jwt-authentication/) Authentication.

### Logging

The app doesn't have a setup to log it's warning and errors to stdout. Looking
into https://symfony.com/doc/current/logging.html might be a good idea.
